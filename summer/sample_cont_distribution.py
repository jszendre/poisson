import time
import numpy as np
from scipy.integrate import quad
from functools import reduce

# Used to find x, st. cdf(x) = random()
def linesearch_increasing(f, a, b, val, tol=1e-6, max_iters=1000):
    # Necessary?
    if np.abs(f(a)-val) < tol: return a
    if np.abs(f(b)-val) < tol: return b
    
    size = (b - a) / 2
    c = (a + b) / 2
    k = 0

    while k<max_iters:
        if np.abs(f(c) - val) < tol:
            return c
        elif f(c) < val:
            c += size / 2
        else:
            c -= size / 2
        size /= 2
        k += 1

    return c

def random_sample_con(pdf, a, b, shape=1):
    """
    This is how you can take random samples from any continuous distribution
      not specified in scipy or numpy or elsewhere.
    Check for stability!!!
    :param pdf:
    :param a: [a, b] interval on which pdf is defined.
    :param b: [a, b] interval on which pdf is defined
    :param shape: A tuple like (2,3,4)
    :return:
    """
    # we search up to 4 standard deviations beyond the mean
    # mean and standard dev. of exponential distribution are 1/l each
    cdf = lambda x:quad(pdf, a, x)[0] # integrates the pdf so we can take random samples.
    x_ = np.random.random() # uniform random float in [0,1)

    # most cases like this
    if type(shape) == type(1) and shape==1:
        return linesearch_increasing(cdf,a,b,x_)

    total_size = reduce(lambda x,y:x*y,shape)
    result = np.fromiter((linesearch_increasing(cdf,a,b,np.random.random()) for x in range(total_size)), np.float)
    result = np.reshape(result, shape)

    return result

#random_sample_con(lambda x:.5*np.exp(-.5*x),0,100,(20,20))
