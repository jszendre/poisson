from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score, KFold
import numpy as np

def fit_exponential_demand(x,y):
    lr = LinearRegression(True)
    X = np.column_stack((x, np.exp(x)))
    lr.fit(X,y)
    eye = np.eye(len(X))

    intercept = lr.predict(np.zeros((len(X))))
    linear = [lr.predict(eye[i])[0][0]-intercept for i in range(len(X))]

    print(intercept, linear)


X = np.random.random((10,))
y = 3.7*np.exp(X) + X

fit_exponential_demand(X,y)
