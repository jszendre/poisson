
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
from scipy.stats import gamma # gamma(a, begin_time, b)

def sample(intensity_fct, a, b, sup_lambda):
    x_ = a
    arrivals = []
    while x_ < b:
        x_ += -np.log(np.random.random()) / sup_lambda
        if np.random.random() < intensity_fct(x_) / sup_lambda:
            arrivals.append(x_)
    return arrivals[:-1]

def demonstrate_thinning():
    scale = 10
    intensity_fct = lambda x:scale*np.exp(1 - .7*x)
    a, b = 0, 10
    arrivals = sample(intensity_fct, a, b, intensity_fct(0))
    n_arrivals = len(arrivals)
    
    X = np.linspace(a,b,600)

    plt.plot(X, intensity_fct(X))
    plt.plot(X, [n_arrivals/(b-a)]*600)
    plt.title("arrivals generated from lambda(t) = 10*e^(1-t))")
    plt.plot(arrivals, [n_arrivals/(b-a)]*n_arrivals, "*", ms=6)
    plt.legend()
    plt.show()

def demonstrate_estimator(estimator):
 
    i=1
    for scale in [1, 10, 100, 1000]:
        plt.subplot(2,2,i); i+=1
        a = 0
        b = 10

        intensity_fct = lambda x:scale*np.exp(1 - .7*x)

        arrivals = sample(intensity_fct, a, b, intensity_fct(0))
        while len(arrivals) == 0:
            arrivals = sample(intensity_fct, a, b, intensity_fct(0))

        n_arrivals = len(arrivals)
        X = np.linspace(a,b,600)

        r = (b-a)/(n_arrivals)**(0.5)
        est = estimator(arrivals, a, b, r)

        plt.plot(np.linspace(a,b,600), list(map(est, np.linspace(a,b,600))))

        plt.plot(X, intensity_fct(X))
        plt.plot(X, [n_arrivals/(b-a)]*600)
        plt.title("scaled by: %d" % scale)
        plt.plot(arrivals, [n_arrivals/(b-a)]*n_arrivals, "*", ms=2, label="r = %.3f"%r)
        plt.legend()
    plt.show()
    
    
def generate_data():
    parameters = ["c0", "c1", "difference", "holiday"]
    weights = np.array([.3465, -np.log(3)])

    initial_features = [0, 0]

    def next_feature(x):
        dx = np.random.normal(0, .02, 2) 

        result = np.array(x) + dx
        result[1] = 1 if np.random.random() < .05 else 0 
        if np.abs(result[0]) > 10:
            result[0] = np.sign(result[0]) * 9.98
        return result

    from scipy.stats import poisson

    def demand(x):
        l = 10 * np.exp(1-weights.dot(x))
        return poisson(l).rvs()

    X = [initial_features]
    N = 10000

    for i in range(N):
        X.append(next_feature(X[-1]))

    y = [demand(x) for x in X]

    return X, y
def show_data(x, y):
    for i in range(10):
        print(x[i], y[i])


