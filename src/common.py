import numpy as np

# meant to turn a cumulative distribution function into a step function
def step_xform(lam, delta=1, a=0):
    return lambda t: lam(delta * np.trunc((t - a) / delta))
