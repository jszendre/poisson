from numpy.polynomial import polynomial as P
import numpy as np
from numpy import array as nd
from functools import reduce
from poisson.realize_process import PoissonGenerator
from matplotlib import pyplot as plt
from scipy.stats import poisson
r = poisson(3)
r.pmf(3)

def epachnikov_kernel(h):
    return lambda x:.75*(1-(x/h)**2)


def uniform_kernel() -> None:
    pass

kernel_functions = ["uniform", \
                    "epachnikov"]

# Use of np.polynomial.polynomial
# print(P.polyval(2, np.array([1,2,3])))
# print(P.polyfit([0,1,2],[0,.75,0],2))

"""Note that as it currently stands, only the second fourth degree option results in a smooth function. It'"""
"""
Issues: 
    1. passing in [0,0,1,2,3,4] will return 
        the same as in passing in [0,1,2,3,4]
    2. In the interval [0,h] the density becomes 
        half of what it should be near 0
    FIX 2.1
        Add f(a-x) to f(a+x)
        Makes E[f_est(0)] = f(0)
        However |f'(0)| < |f'(0)|  
        
    3. diverges to twice the correct value on the right 
        for sin(x) from .2 to pi - .2
"""
def est_kernel_polynomial(x_points, # type: List[int]
                          num_realizations, # type int
                          kernel_fct, h, a, b, one_sided_right=False):
    # type
    """

    :param x_points: the points
    :param num_realizations:
    :param kernel_fct: example lambda x: P.polyfit([x - h, x, x + h], [0, .75, 0], 2)
    :param h: the radius of the kernel function
    :param a: the left endpoint of the distribution
    :param b: the right endpoint of the distribution
    :param one_sided_right: deprecate this
    :return:
    """
    polynomials = [kernel_fct(x_) for x_ in x_points]
    begin = nd([x_ - h*(1-1*one_sided_right) for x_ in x_points])
    end = nd([x_+h for x_ in x_points])

    all_pts = np.unique(np.hstack(([-1e50],begin, end)))
    in_interval = lambda x: np.where(np.logical_and(begin <= x, end > x))[0]
    spline_fcts = [in_interval(x) for x in all_pts]

    zero_polynomial = 0 * kernel_fct(0)
    spline = nd([zero_polynomial] + [reduce(np.add, [polynomials[x] for x in c], zero_polynomial) for c in spline_fcts])

    f = lambda x: P.polyval(x, spline[np.where(all_pts <= x)[0][-1]])

    # FIX 1
    g = lambda x: 0 if x > a + h else P.polyval(2 * a - x, spline[np.where(all_pts <= 2 * a - x)[0][-1]])
    k = lambda x: 0 if x < b - h else P.polyval(2*b-x, spline[np.where(all_pts <= 2*b-x)[0][-1]])

    y_graph = []
    # linspace produces 100 equally spaced points in [a,b]
    for x in np.linspace(a,b,100):
        y_graph.append(f(x)+g(x)+k(x))

    y_graph  = nd(y_graph)
    y_graph /= num_realizations*h

    plt.plot(np.linspace(a,b,100), y_graph)
    return lambda x:f(x)+g(x)+h(x)

a, b = 0, 4

h = .5 # radius of kernel function
c1 = h*np.sqrt((-1/np.sqrt(2)+1)) # used for quartic kernel fct (basis_p2)

basis_p = lambda x: P.polyfit([x - h, x, x + h], [0, .75, 0], 2)
basis_p2 = lambda x: (15/16)*P.polyfit([x - h, x - c1, x, x + c1, x + h], [0, .5, 1, .5, 0], 4)

# Generate 10 realizations of the same process
l_2, max_rate = lambda x:20*np.exp(-x), 20
#l_2, max_rate = lambda x:np.sin(x), 1


gen = PoissonGenerator()
n_realizations = 50
# generate realiizations and concatenate their arrival times together
overlapping = np.hstack((gen.generate(a, b, l_2, max_rate) for _ in range(n_realizations)))
print(overlapping.size/n_realizations)
est_kernel_polynomial(overlapping, n_realizations,
                      basis_p, h, a, b, False)
print("Even with reflecting extra density this kernel estimation is not valid near the endpoints if thenderivative of the rate function is negative (positive) at the left (right) endpoint. Using a nonsymmetric kernel function near the ends could help...")
plt.plot(np.linspace(a,b, 300), list(map(l_2, np.linspace(a,b,300))))

plt.show()
