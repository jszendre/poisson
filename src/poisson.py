from scipy.integrate import quad
from sample_cont_distribution import random_sample_con
import numpy as np


def gen_NHPP_thinning(l, a, b, l_max):
    """ l: is a rate function
        a,b: The open interval on which to sample our process
        l_max: the sup of l over (a,b)
               Would any upper bound work (???) """
    result, x_ = [], a
    while x_ < b:
        # for homogeneous Poisson cdf^-1(x) = -log(x) / l where l is the rate parameter
        x_ += -np.log(np.random.random())/l_max
        
        p = l(x_) / l_max
        if np.random.random() < p and x_ < b:
                result += [x_]

    return result


def example_rate_function():
    # A polynomial over (0,1) with that has value max at the center and is positive
    return lambda x:np.abs(np.sin(.5*x)**8)

if __name__ == "__main__":
    from matplotlib import pyplot as plt
    import pandas as pd
    print("Generating realization for rate function l(x) = sin(.5x)**8 over (0, 1)")
    f = example_rate_function()
    print("A few realizations:")
    plt.plot(np.linspace(0,12,200), f(np.linspace(0,12,200)))
    all_points = []
    for i in range(10):
        realization_i = gen_NHPP_thinning(f, 0, 12, 1)
        plt.plot(realization_i, [.02*i] * len(realization_i), "*", linewidth=10)
        all_points += realization_i
    plt.show()

    for i in range(90):
        realization_i = gen_NHPP_thinning(f, 0, 12, 1)
        all_points += realization_i
    pd.DataFrame({"all_points": list(all_points)}).plot(kind="hist")
    plt.plot(np.linspace(0,12,200), 100*f(np.linspace(0,12,200)))
    plt.title("After generating 100 realizations")
    plt.show()


# Less inefficient way, think it works
def gen_inh_poisson_(l, a, b):
    from scipy.stats import poisson
    l_ = quad(l, a, b)[0]
    num_points = poisson.rvs(l_)
    pdf = lambda x: l(x) / l_
    for i in range(num_points):
        x_ = linesearch_increasing(pdf, a, b, np.random.random(), tol=1e-6, max_iters=30)
        result.append(x_)
    return result

# Used to think this works, not so sure anymore
def gen_inh_poisson_cdf(l, a, b):
    pdf = lambda x:l(x)*np.exp(-l(x)) # not a pdf until normalized
    
    result, x_ = [], a
    while x_ < b:
        # random_sample_con integrates the pdf and finds where integral a to x
        # equals u drawn from a uniform random over (0,1)
        pdf = lambda x:l(x)*np.exp(-l(x)*(x-x_))
        x_ += random_sample_con(lambda x:pdf(x) / quad(pdf, a, b)[0], a, b)
        if x_ < b:
            result.append(x_)
    return result

